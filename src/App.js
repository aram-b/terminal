import React, { useState, useEffect } from 'react';
import FirstPage from './components/FirstPage';
import SecondPage from './components/SecondPage';
import ThirdPage from './components/ThirdPage';

function App() {
	const [showpage, setShowpage] = useState(1);
	const [error, setError] = useState(false);
	const [outcomeCounter, setOutcomeCounter] = useState(
		{
			wins: 0,
			losses: 0,
			ties: 0
		}
	);
	const [message, setMessage] = useState();

	function handleSubmit (e) {
		e.preventDefault();
		if (e.target['wachtwoord'].value === 'gadofriesdriedoezoe') {
			setShowpage(2)
		} else {
			setError(true)
		}
	}

	useEffect(() => {
		if (outcomeCounter.wins === 1) {
			setShowpage(3)
			setMessage([
				'Je hebt gewonnen',
				'Knap gedaan',
				'Je hebt je prijs verdiend',
				'Check je mail',
				'Tot de volgende keer...'
			])
		} else if (outcomeCounter.losses === 5) {
			setShowpage(3)
			setMessage([
				'Het wil niet echt lukken, he?',
				'Nou ja',
				'Je hebt je best gedaan',
				'Je hebt je prijs verdiend',
				'Check je mail',
				'Tot de volgende keer...'
			])
		} else if (outcomeCounter.ties === 3) {
			setShowpage(3)
			setMessage([
				'Vreemd spel',
				'De enige manier om te winnen',
				'Is door niet te spelen',
				'Je hebt je prijs verdiend',
				'Check je mail',
				'Tot de volgende keer...'
			])
		}
	}, [outcomeCounter])

	return (
		<div className="App">
			{showpage === 1 ? <FirstPage handleSubmit={handleSubmit} error={error}/> : null }
			{showpage === 2 ? <SecondPage outcomeCounter={outcomeCounter} setOutcomeCounter={setOutcomeCounter}/> : null }
			{showpage === 3 ? <ThirdPage message={message} /> : null }
		</div>
	);
}

export default App;