import styled from 'styled-components';

const Input = styled.input`
	border: ${props => props.error ? '2px solid red' : '2px solid #7fff7f'};
	background-color: transparent;
	color: #7fff7f;
	font-family: monospace;
	font-size: 30px;
	padding-left: 10px;
	padding-right: 10px;
	width: 130px;
	outline: none;
`

export default Input;