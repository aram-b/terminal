import React, { useState, useEffect } from 'react';
import Text from './Text';
import Input from './Input';
import Button from './Button';

function FirstPage({ handleSubmit, error }) {
	const [counter, setCounter] = useState(0);

	const lines = [
		'Hallo Godfried',
		'De Sint wil een spelletje spelen',
		'Maar eerst moet je toegang zien te krijgen',
		'Ga naar de bibliotheek op de eerste verdieping',
		'En kijk onder de bureaustoel',
		'Ik wacht...'
	]

	useEffect(() => {
		const timer = setTimeout(() => {
			if (counter + 1 < lines.length) {
				setCounter(counter + 1)
			}
		}, 4000);
		return () => clearTimeout(timer);
	}, [counter, lines])

	return (
		<>
			<Text key={counter}>{lines[counter]}</Text>
			{counter + 1 === lines.length ?
			<form onSubmit={handleSubmit}>
				<Input type='password' id='wachtwoord' error={error} />
				<Button type='submit'>Volgende ronde</Button>
			</form> :
			null}
		</>
	);
}

export default FirstPage;