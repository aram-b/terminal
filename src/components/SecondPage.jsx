import React, { useState, useEffect } from 'react';
import Text from './Text';
import TicTacToe from './TicTacToe';

const SecondPage = ({ outcomeCounter, setOutcomeCounter }) => {
	const [counter, setCounter] = useState(0);
	const [outcome, setOutcome] = useState('?');
	const [message, setMessage] = useState();

	const lines = [
		'Heel goed',
		'Maar de volgende opdracht wordt niet zo makkelijk',
		'Hou je van spelletjes, Godfried?',
		'Vast wel',
		'Als je het volgende spelletje wint',
		'Ben je weer een stap dichterbij de prijs',
		'Laten we spelen',
		''
	]

	useEffect(() => {
		const timer = setTimeout(() => {
			if (counter + 1 < lines.length) {
				setCounter(counter + 1)
			}
		}, 4000);
		return () => clearTimeout(timer);
	}, [counter, lines])

	useEffect(() => {
		if (outcome === 'X') {
			setOutcomeCounter(
				{
					...outcomeCounter,
					wins: outcomeCounter.wins + 1
				}
			);
			setOutcome('?');
			setMessage(<Text>Gewonnen</Text>);
		} else if (outcome === 'O') {
			setOutcomeCounter(
				{
					...outcomeCounter,
					losses: outcomeCounter.losses + 1
				}
			);
			setOutcome('?');
			setMessage(<Text key={outcomeCounter.losses}>Verloren, probeer het nog eens</Text>);
		} else if (outcome === 'C') {
			setOutcomeCounter(
				{
					...outcomeCounter,
					ties: outcomeCounter.ties + 1
				}
			);
			setOutcome('?');
			setMessage(<Text key={outcomeCounter.ties}>Gelijkspel, probeer het nog eens</Text>);
		}
	}, [outcome, outcomeCounter, setOutcomeCounter])

	return(
		<>
			{counter+1 !== lines.length ?
			<Text key={counter}>{lines[counter]}</Text> :
			<>
				{message}
				<TicTacToe
					key={outcomeCounter.wins + outcomeCounter.losses + outcomeCounter.ties}
					setOutcome={setOutcome}
				/>
			</>}
		</>
	)
}

export default SecondPage;