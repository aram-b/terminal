import React, {useState, useEffect} from 'react';
import Text from './Text';
import emailjs from 'emailjs-com';

const ThirdPage = ({ message }) => {
	const [counter, setCounter] = useState(0);

	useEffect(() => {
		const timer = setTimeout(() => {
			if (counter + 1 < message.length) {
				setCounter(counter + 1)
			}
		}, 4000);
		return () => clearTimeout(timer);
	}, [counter, message])

	useEffect(() => {
		if (counter === 1) {
			emailjs.send('default_service', 'sint', {}, 'user_bg98SFQ4rQ5l9lao9kovL')
			.then(function(response) {
				console.log('SUCCESS!', response.status, response.text);
			}, function(error) {
				console.log('FAILED...', error);
			});
		}
	}, [counter])

	return(
		<Text key={counter}>{message[counter]}</Text>
	)
}

export default ThirdPage;