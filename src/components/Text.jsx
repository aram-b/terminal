import styled, { keyframes } from 'styled-components';

const blink = keyframes`
	from, to { border-color: transparent; }
	50% { border-color: #7fff7f; }
`

const typing = keyframes`
	from { width: 0 }
	to { width: 100% }
`

const Text = styled.p`
	overflow: hidden;
	border-right: 0.5em solid #7fff7f;
	white-space: nowrap;
	letter-spacing: 0.1em;
	animation: 
		${typing} 1.5s steps(40, end),
		${blink} .75s step-end infinite;
`

export default Text;