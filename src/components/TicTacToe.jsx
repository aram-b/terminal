import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

const TicTacToe = ({ setOutcome }) => {

	const [game, setGame] = useState('---------');
	const [autoInput, setAutoInput] = useState(false);

	useEffect(() => {
		if (autoInput) {
			fetch("https://stujo-tic-tac-toe-stujo-v1.p.rapidapi.com/" + game +"/O", {
				"method": "GET",
				"headers": {
					"x-rapidapi-host": "stujo-tic-tac-toe-stujo-v1.p.rapidapi.com",
					"x-rapidapi-key": "ca5892e701msh9e968b2ffb77c02p120791jsn815400b55f5e"
				}
			})
			.then(response => response.json())
			.then(json => {
				setAutoInput(false);
				const newGame = game.substr(0,json.recommendation) + 'O' + game.substr(json.recommendation).substr(1);
				setGame(newGame);
			})
			.catch(err => {
				console.log(err);
			});
		}
	}, [game, autoInput])

	const handleClick = (evt) => {
		const index = evt.target.value;
		if (game.substr(index, 1) === '-') {
			const newGame = game.substr(0,index) + 'X' + game.substr(index).substr(1);
			setGame(newGame);
			setAutoInput(true);
		}
	}

	const StyledButton = styled.button`
		background-color: transparent;
		border: 2px solid #7fff7f;
		color: #7fff7f;
		width: 45px;
		height: 45px;
		font-size: 20px;
		outline: none;
	`

	const Square = ({ number, children }) => (
		<StyledButton
			onClick={handleClick}
			value={number}
		>
			{children}
		</StyledButton>
	)

	useEffect(() => {
		function game_overQ() {
			return winnerQ(0,1,2)  // check for 3-in-a-row horizontally
				||  winnerQ(3,4,5) 
				||  winnerQ(6,7,8) 
				||  winnerQ(0,3,6)  // check for 3-in-a-row vertically
				||  winnerQ(1,4,7) 
				||  winnerQ(2,5,8) 
				||  winnerQ(0,4,8)  // check for 3-in-a-row diagonally
				||  winnerQ(6,4,2)
				||  stalemateQ();   // check for win by 'cat'
		}

		function winnerQ(p1, p2, p3) {
			var s = game;
			var c1 = s.charAt(p1);
			if (c1 === '-') return false;
			var c2 = s.charAt(p2);
			if (c1 !== c2) return false;
			var c3 = s.charAt(p3);
			if (c1 !== c3) return false;
			setOutcome(c1);
			return true;
		}

		function stalemateQ()
		{
			var s = game;
			for (var i=0; i<9; i++) {
				if (s.charAt(i) === '-') return false;
			}
			setOutcome("C");
			return true;
		}

		game_overQ();
	}, [game, setOutcome])

	return(
		<div>
			<div>
				<Square number={0}>{game.charAt(0)}</Square>
				<Square number={1}>{game.charAt(1)}</Square>
				<Square number={2}>{game.charAt(2)}</Square>
			</div>
			<div>
				<Square number={3}>{game.charAt(3)}</Square>
				<Square number={4}>{game.charAt(4)}</Square>
				<Square number={5}>{game.charAt(5)}</Square>
			</div>
			<div>
				<Square number={6}>{game.charAt(6)}</Square>
				<Square number={7}>{game.charAt(7)}</Square>
				<Square number={8}>{game.charAt(8)}</Square>
			</div>
		</div>
	)
}

export default TicTacToe;