import styled from 'styled-components';

const Button = styled.button`
	border: 2px solid #7fff7f;
	background-color: transparent;
	display: block;
	color: #7fff7f;
	font-family: monospace;
	font-size: 20px;
	margin-top: 20px;
	cursor: pointer;
`

export default Button;